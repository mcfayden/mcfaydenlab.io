---
title: "First FASER results!"
date: 2023-03-31T12:52:36+06:00
#image_webp: images/blog/blog-post-3.webp
image: images/blog/FASERv_firstNu_2D.png
author: Josh McFayden
description : "First detection of collider neutrinos and first Dark Matter search from FASER"
---

**A big milestone!**

Last week the FASER collaboration announced the first direct observation of collider neutrinos and has released its first results in the search for Dark Matter.

FASER is one of two new experiments at the LHC searching for neutrinos produced in proton collisions at the ATLAS experiment at CERN in Geneva, Switzerland.

I joined FASER in 2019 and have contributed to the construction and commissioning of the scintillator and calorimeter sub-detectors.

**So what's this all about?**

There are a billion ghost-like particles passing through an area the size of your thumbnail every second. They are particles known as neutrinos that play a crucial role in our understanding of the Universe, from how the Sun shines, to exploding supernovae.

There are also lots of open questions about neutrinos - we know they have mass, but our best theory of fundamental particles and their interactions (the Standard Model) predicts them to be massless. FASER is adding to the long history of neutrino physics by being the first ever experiment to observe directly neutrinos produced in a collider (the LHC).

I am very happy to see these first results from the FASER experiment, presented at [Moriond EW](https://indico.in2p3.fr/event/29681/timetable/#20230319.detailed) last week, coming four years after my initial involvement. It’s especially rewarding because FASER is a new type of experiment at the Large Hadron Collider at CERN, and today’s results are proof that this type of experiment can work, so it really breaks new ground.

Previous examples of neutrino detection from different sources has led to profound insights across the fields of particle physics, nuclear physics, and astrophysics.
**What will we learn with these new results?!** Well, new measurements from FASER offer the prospect of opening up a new window to physics beyond the Standard Model to study these mysterious particles at the highest energy produced by humankind.


FASER stands for “Forward Search Experiment” and one of it's other aims is to search for exotic new particles that are excellent candidates to explain the existence of Dark Matter. If they exist, these exotic particles would be produced in collisions inside the ATLAS detector and be detected nearly 500m away in FASER.

FASER is able to look for a type of these Dark Matter particles that no experiment until now has had sensitivity to. Unfortunately, the result released today did not show any signs of Dark Matter, but it was able to prove that FASER is one of the best tools to look for it when more LHC data comes!

As well as working on FASER, I'm working with colleagues at Sussex on R&D for a possible successor to FASER that is being proposed for construction in around 2030.


**Find more information in the following links:**

[CERN News Article](https://home.cern/news/news/experiments/new-lhc-experiments-enter-uncharted-territory)

[Sussex News Article](https://www.sussex.ac.uk/physics/about/newsandevents/index?id=60583)

[FASER Neutrino Paper arXiv](https://arxiv.org/abs/2303.14185)

[FASER Dark Photon Public note](https://cds.cern.ch/record/2853210)
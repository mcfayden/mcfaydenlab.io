---
title: "First differential ttW measurement"
date: 2023-03-31T12:42:36+06:00
#image_webp: images/blog/blog-post-3.webp
image: images/blog/ttW-diff.png
author: Josh McFayden
description : "ATLAS makes the first differential measurement of the very challenging ttW final state"
---

**The hardest measurement I've done!**

This has been a slog. Since a [measurement in 2019 of ttH production](https://atlas.web.cern.ch/Atlas/GROUPS/PHYSICS/CONFNOTES/ATLAS-CONF-2019-045/) we've known that there's something interesting in the data in measurements enriched in ttW production.

After the result in 2019 we decided that we needed to measure this thing properly. But this is very hard as it's hard to get data pure in ttW, estimating the backgrounds is very challenging, and understanding the theoretical predictions is also very difficult as ttW is a very complex process phenomenologically.

But last week for [Moriond EW](https://indico.in2p3.fr/event/29681/timetable/#20230319.detailed) our measurement was finally ready. We don't see any major tension with the theoretical predictions once they have been updated to the state-of-the-art but the measurements are still quite statistically limited, so it will be important to repeat them with more data from LHC Run 3.

**Find more information in the following links:**

[ATLAS Briefing](https://atlas.cern/Updates/Briefing/ttW-Mild-Tension)

[Public Note](https://atlas.web.cern.ch/Atlas/GROUPS/PHYSICS/CONFNOTES/ATLAS-CONF-2023-019/)

---
title: "Forward Physics Facility Workshop"
date: 2023-06-09T12:53:36+06:00
image: images/blog/FPF6.png
author: Josh McFayden
description : "FPF6"
---

**FPF6**

The 6th Forward Physics Facility meeting took place at CERN (in hybrid mode) earlier this June.
The meeting was a bit step forward for the facility with both news from civil engineering works (core drilling to assess the site suitability) and defining the timeline and milestones for the next steps in trying to get the facility approved.

I presented an update for FASER2 where there has been much progress, primarily on understanding the magnet design constraint and the development of a tracking reconstruction algorithm.


**Find more information in the following links:**

[6th Forward Physics Facility Meeting](https://indico.cern.ch/event/1275380)

---
title: "HSF Monte Carlo Tuning Workshop"
date: 2023-06-27T12:00:00+06:00
image: images/blog/2023Tuning.png
author: Josh McFayden
description : "FPF6"
---

**HSF Monte Carlo Tuning Workshop**

I was one of the organisers of an online-only workshop organised through the [HEP Software Foundation](https://hepsoftwarefoundation.org/) (HSF) on Monte Carlo (MC) Event Generator Tuning. MC event generators are the tools we use to prediction the probability and nature of particle collisions at, for example, the Large Hadron Collider. They are a key part in our search to measure fundamental particles and to look for new physics as they form the basis of the predictions which we compare our data to. But getting the MG event generators to give the best possible representation of the data requires them to be "tuned" i.e. tunable parameters which are not known a priori need to be optimised to best reproduce the data (in regions where we do not expect new physics!).

The idea of this workshop was twofold. First, to bring together different groups in particle physics (collider physics, nuclear physics and neutrino physics) to share experiences and ideas for the benefit of all. Second, to provide training to ensure that existing expertise is passed on and to ensure there is continued support and progress in this area.

On the first day, we started with an overview of tuning MCEGs presented by Peter Skands (Monash University). Following this, representatives from various experimental collaborations, ALICE, ATLAS, CMS, ePIC, LHCb, and neutrino experiments, shared their tuning experience. The session ended with a hands-on session, delving into how different parameters influence observable distributions in Pythia.

The second day featured an insightful overview of semi-automated tuning by Andy Buckley (University of Glasgow). This was followed by a community discussion, fostering knowledge exchange and collaboration. At the end of the workshop, we engaged in a hands-on tuning tutorial using RIVET and Professor.


There were nearly 90 participants and the tutorials were attended by approximately 50 people. There was much lively discussion and the workshop was a great success, we plan to hold another in the coming months!


**Find more information in the following links:**

[Indico Agenda](https://indico.cern.ch/event/1283969/)

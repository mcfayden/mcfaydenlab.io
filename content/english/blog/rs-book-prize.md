---
title: "Royal Society Science Book Prize"
date: 2022-11-30T12:42:36+06:00
#image_webp: images/blog/rs-book-prize-me.jpg
image: images/blog/rs-book-prize-me.jpg
author: Josh McFayden
description : "Royal Society Science Book Prize"
---

**"The Booker Prize of science writing"**

I was one of five on the judging panel for the high profile [Royal Society Science Book Prize 2022](https://royalsociety.org/news/2022/11/book-prize-2022/).

I found the judging experience challenging (there are a lot of books the get through!), but incredibly rewarding. It was inspirational in many ways and reassuring that good science writing is alive and well at a time when we need it most! The whole process was a real privilege to be part of.

The award ceremony was at the Royal Society on the 19th November, it was really fun to go and meet the authors in person. The winner was Henry Gee's _A (Very) Short History of Life on Earth: 4.6 Billion Years in 12 Chapters_. It’s not often that you finish reading a book feeling shell shocked... but that’s the impact the book had on me! A thoroughly deserving winner! 


**Find more information in the following links:**

[Radio 4 interview](https://www.bbc.co.uk/sounds/play/m001fmtb)

[Sussex Staff News](https://staff.sussex.ac.uk/news/article/59458-research-fellow-josh-mcfayden-on-judging-panel-for-2022-royal-society-science-book-prize)

[Royal Society Page](https://royalsociety.org/news/2022/11/book-prize-2022/)
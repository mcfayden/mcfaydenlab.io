---
title: "ttW discrepancy remains!"
date: 2023-06-09T12:52:36+06:00
image: images/blog/ttW-NNLO.png
author: Josh McFayden
description : "Interesting discrepancy in ttW remains after NNLO predictions"
---

**Tension in ttW remains after new predictions!**

At the recent [LHC Top Working Group meeting](https://indico.cern.ch/event/1254906/) the latest ttW measurements were presented, along with a new state-of-the-art NNLO calculation.

**Find more information in the following links:**

[LHC Top WG - Experimental talk](https://indico.cern.ch/event/1254906/contributions/5410386/attachments/2661181/4610377/LHC_Top_WG_ttW_latest_results.pdf)

[LHC Top WG - ttW NNLO calculation talk](https://indico.cern.ch/event/1254906/contributions/5410388/attachments/2661217/4611242/ttW_talk_TOPWG_Savoini.pdf)

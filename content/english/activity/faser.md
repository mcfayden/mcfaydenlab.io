+++
title = "FASER"
#date = "2018-09-12T14:51:12+06:00"
#author = "Josh"
#image = "images/activity/faser_diagram.png"
image = "images/activity/faser_render_cropped.png"
#image = "https://faser.web.cern.ch/sites/faser.web.cern.ch/files/2019-12/faser_detector_rendering.jpg"
description = "[**FASER**](https://faser.web.cern.ch/) is \"`F`orw`A`rd `S`earch `E`xpe`R`iment\"."
+++


## About FASER
`UNDER CONSTRUCTION`

### Detector
Below is an `interactive display` of the FASER detectors and a candidate event using the [PHOENIX](https://github.com/HSF/phoenix) event visualisation software:
{{< iframe "https://joshmcfayden.github.io/phoenix/#/faser" "500" >}}

Here is `schematic diagram` giving a little more detail about the FASER detector components:
{{< figure src="../../images/activity/faser_diagram.png" style="width:100px;" >}}

### Location
`UNDER CONSTRUCTION`
{{< youtube "XUzbLDQU-PU?autoplay=on" >}}


## Scintillator
`UNDER CONSTRUCTION`

## Calorimeter
`UNDER CONSTRUCTION`

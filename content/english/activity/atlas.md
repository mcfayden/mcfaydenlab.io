+++
title = "**ATLAS**"
#date = "2018-09-12T14:51:12+06:00"
#author = "Josh"
image = "images/activity/me_atlas2.webp"
description = "[ATLAS](http://atlas.ch) stands for \"`A` `T`oriodal `L`HC `A`pparatu`S`\", it is one of the worlds most complex and ground-breaking scientific experiments. The ATLAS Collaboration is an international team of over 3000 scientists that oversee ATLAS, one of the major experiments at the Large Hadron Collider (LHC) at CERN."
+++

Our current best understanding of fundamental particles and their interactions, the Standard Model (SM), has been incredibly successful in its predictions so far. Yet we know it to be an incomplete theory, and therefore expect new physics to exist. Discovering this new physics is the primary goal of ATLAS and the LHC. As a member of the ATLAS Collaboration I have made significant contributions to one of the worlds most complex and important scientific experiments. ATLAS is an international collaboration responsible for one of the major experiments at the Large Hadron Collider (LHC) at CERN. ATLAS is pushing the frontiers of knowledge seeking answers to fundamental questions about our Universe through precision measurements that unlock the potential discovery of new phenomena.

## Short career overview

I began my career working on searches for Supersymmetry (top & bottom squarks) and Exotic resonances decaying to a Higgs boson (H→bb̄) and a Vector boson (VH), both in final states involving b-quark jets. To further improve the sensitivity of these analyses I went on to investigate the dominant backgrounds (and therefore uncertainties).

V+bb̄ is the dominant background for SM and exotic VH production and I improved the Monte Carlo modelling of V+(b)jets processes by upgrading the ATLAS-wide simulated samples to improved precision and performed precision measurements of the production of b-hadron pairs, Z+jets and Z+b-jets, to further constrain and improve the MC modelling. In 2016 I was made convener of the ATLAS Physics Modelling Group, a large working group at Physics Coordination level in ATLAS, where I have pushed forward major improvements in the precision and accuracy of MC simulations across many processes. 

Top-quark pair production in association with a vector boson (tt̄V) is an irreducible background to top squark searches. Following on from my SUSY searches I initiated and led the first ever tt̄Z search on ATLAS and am still responsible for the tt̄V Monte Carlo (MC) samples and the corresponding systematic uncertainties. I also played a leading role in the 7 and 8 TeV tt̄V publications. tt̄V is also a dominant background to measurements of Higgs production in association with a top-quark pair (tt̄H) whether the Higgs decays to multi-lepton final states (ttHML). Therefore, I transferred my expertise to ATLAS ttHML measurements which is now a major focus of my research.

In the long term I am excited about the huge potential in the High-Luminosity LHC physics program. I am already proactive in this area as a member of the ECFA Early Career Researchers Board providing input to the European Strategy Group Strategy Update. I am also Convener of the HEP Software Foundation (HSF) Event Generators Working Group where I am working to improve current & future MC generation software & HPC workflows. 


## Higgs

Discovery of Beyond the Standard Model (BSM) physics has proven elusive and the hopes of the LHC programme are, in this respect, yet to be fully realised. The discovery of the Higgs boson in 2012 (which led to the 2013 Nobel Prize) is one of the major scientific achievements of this millennium. Probing the Higgs boson, as the most recently discovered fundamental particle, and one which is unlike anything else in the Standard Model (SM) as the only elementary scalar, is of critical interest and highest priority in the search for new physics at ATLAS and more widely at the LHC.

My research is focussed on Higgs boson production in association with a top-quark pair (tt̄H) which provides one of the most challenging but most exciting and high priority areas for investigation. The top quark has the largest coupling to the Higgs and is therefore a place where BSM physics may most clearly be manifested if the observed Higgs is non-SM. In addition, precise measurement of tt̄H production is sensitive to the Higgs self-coupling which even without BSM physics is a parameter of huge interest (more infomation below).

In particular, my focus is on tt̄H production in collisions where the Higgs decays to multiple leptons, *ttHML*. The leptonic decay channels are a vital input to the overall tt̄H sensitivity with the existing Run 2 (140 fb-1 at √s=13 TeV) and upcoming (2021-2023) Run 3 (300 fb-1 at √s=14 TeV) datasets.

The latest result of my work is an ATLAS Conference Note which I oversaw the publication of in August. More details on this are given [here](#ttH_ttW).






## Top

The LHC is often referred to as a top quark factory, the LHC has so far produced more than 100 million top quark pairs, that's more than 100 thousand per day when the LHC is running! As the heaviest fundamental particle and the only quark that decays before hadronising the top is an extremely interesting particle in its own right. My particular interest is in more rare processes where a top quark pair is produced in association with a vector boson (a W or Z boson).

I initiated and was Analysis Coordinator for the first ever ttZ measurement on ATLAS, I also played a leading role in the subsequent 7 and 8 TeV ttV publications and am now working on differential ttW measurements at 13 TeV. I am also responsible for the ttV Monte Carlo (MC) samples and the corresponding systematic uncertainties which are used throughout ATLAS.


## The Higgs, the top and the ugly (the fate of the Universe)

The interplay between top quark and the Higgs boson is not just important for understanding these particles better, but is also intrinsically linked to the quantum stability of the Universe and therefore a key part of understanding our existence. In particular, the strength of the Higgs boson's coupling to itself and to the top quark are key parameters in determining the shape of the Higgs potential. These parameters define whether the current "ground state" or minimum of the Higgs potential is the true minimum and the universe is stable or whether it is a false minimum and the universe is unstable. Current data indicates we are between the two, in a meta-stable state. This is arguably the most important message from the LHC so far and can be interpreted to mean new physics must come in at higher energy scales to restore stability. This cutting edge research is at the intersection between particle physics and cosmology where the precise determination of Higgs boson-top quark interactions is vital in understanding our existence and searching for new particles. 

My specific focus in this area is in the determination of the top-Higgs and Higgs self couplings in tt̄H production. The Higgs self-coupling can be measured both in d-Higgs and single-Higgs production at the LHC. Each process gives great complementarity and the highest sensitivity comes through a combination of both approaches. In tt̄H production the Higgs self coupling is accessible through differential measurement of these events. This is very challenging with the low tt̄H cross section, but with LHC Run 2 and Run 3 data we are just reaching the point where this is possible.


## MC Generators

Physics event generators are essential for HEP analyses. All of the LHC scientific results, such as precision measurements or searches for new physics, are based on the comparison of the experimental measurements to theoretical predictions computed using generator software. 

Using Monte Carlo (MC) techniques, generators provide estimates of the probability (cross section) for different collisions to take place at the LHC. Within ATLAS event generators are the first step in the chain for simulated LHC collisions, which is followed by detector simulation and event reconstruction.

The theoretical unknowns that exist in MC event generation is a source of major uncertainty in many analyses and can therefore limit the potential for the precision of measurements and the sensitivity of searches, so reducing these uncertainties is a high priority for ATLAS.

I am responsible for introducing state-of-the-art MC tools across the ATLAS physics program and have a strong record of identifying MC modelling issues then defining and contributing to measurements to drive improvements. I am an expert on physics & technical matters, especially for the  MadGraph5_aMC@NLO generator. I was the first person on ATLAS to implement several state-of-the art MC tools: MadGraph5_aMC@NLO merging schemes, CKKW-L, FxFx. Geneva generator. My work on e.g. V+jets MC samples and has resulted in improved modelling and reduced uncertainties for several major ATLAS MC samples. 



## Publications

Here is a bit more in-depth information about some of my most important publications.


### ttH

{{< anchor ttH_ttW >}}
#### Analysis of `ttH` and `ttW` production in `multilepton` final states with the ATLAS detector
[ATLAS-CONF-2019-045](https://atlas.web.cern.ch/Atlas/GROUPS/PHYSICS/CONFNOTES/ATLAS-CONF-2019-045/)

{{< button Document "https://atlas.web.cern.ch/Atlas/GROUPS/PHYSICS/CONFNOTES/ATLAS-CONF-2019-045/" >}}
{{< button Figures "https://atlas.web.cern.ch/Atlas/GROUPS/PHYSICS/CONFNOTES/ATLAS-CONF-2019-045/" >}}


**Abstract:**
*A search for the associated production of a top-quark pair with the Higgs boson (ttH) in multilepton final states is presented. The search is based on a dataset of proton--proton collisions at √s=13TeV recorded with the ATLAS detector at the CERN Large Hadron Collider and corresponding to an integrated luminosity of 80 fb−1. Six final states, defined by the number and flavour of charged-lepton candidates, and 25 event categories are defined to simultaneously search for the ttH signal and constrain several leading backgrounds. The ttW background normalisation is left unconstrained in the statistical analysis and the resulting ttW normalisation is found to be higher than the theoretical prediction. An excess of events consistent with ttH production, over the expected background from Standard Model processes, is found with an observed significance of 1.8 standard deviations, compared to an expectation of 3.1 standard deviations. Assuming Standard Model branching fractions, the best-fit value of the ttH production cross section is σ(ttH)=294 +182/−162 fb, which is consistent with the Standard Model prediction. The impact on the ttH cross section measurement of the assumptions made on the ttW background modelling is discussed.*

**My contributions:**
I was analysis coordinator